package com.roshandel.start;

import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.ibm.icu.util.ULocale;

import java.util.Date;

public class Start {
    public static void main(String[] args){
        System.out.println("It's working...");
        Person person = new Person();

        person.setFirstName("AmirHossein");
        person.setLastName("Roshandel");

//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(new Date());
//        calendar.set(2000, 2, 6);
//        person.setBirthDate(calendar.getTime());

        final ULocale PERSIAN_LOCALE = new ULocale("fa_IR@calendar=persian");
        Calendar persianCalender = Calendar.getInstance(PERSIAN_LOCALE);
//        persianCalender.set(Calendar.DAY_OF_MONTH, 17);
        persianCalender.set(Calendar.MONTH, 10, 17);
        persianCalender.set(Calendar.YEAR, 1378);

        person.setBirthDate(persianCalender.getTime());
        String shamsi = new SimpleDateFormat("yyyy/MM/dd", PERSIAN_LOCALE).format(person.getBirthDate());


        System.out.println(person.getFirstName());
        System.out.println(person.getLastName());
        System.out.println(shamsi);
    }
}
